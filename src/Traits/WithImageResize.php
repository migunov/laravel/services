<?php

namespace Migunov\Services\Traits;

trait WithImageResize
{
    public static function resize(
        string $path,
        int $width = 1200,
        int $height = 600,
        ?string $targetPath = null
    ): void {
        /** @var array */
        $result = self::initImage($path, $targetPath);

        if (!$result) {
            return;
        }

        $image = $result['image'];
        $target = $result['target'];

        list($width, $height) = self::initSize($image, $width, $height);

        if ($image->width() >= $image->height()) {
            $image->scale(width: $width)->save($target, 90);
        } else {
            $image->scale(height: $height)->save($target, 90);
        }
    }
}
