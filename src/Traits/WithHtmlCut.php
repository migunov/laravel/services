<?php

namespace Migunov\Services\Traits;

trait WithHtmlCut
{
    public static function htmlCut($text, $max_length): string
    {
        $tags = [];
        $result = '';

        $is_open = false;
        $grab_open = false;
        $is_close = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = '';

        $i = 0;
        $stripped = 0;

        $stripped_text = strip_tags($text);

        while (
            $i < mb_strlen($text)
            && $stripped < mb_strlen($stripped_text)
            && $stripped < $max_length
        ) {
            $symbol = mb_substr($text, $i, 1);
            $result .= $symbol;

            switch ($symbol) {
                case '<':
                    $is_open = true;
                    $grab_open = true;

                    break;

                case '"':
                    $in_double_quotes = !$in_double_quotes;

                    break;

                case "'":
                    $in_single_quotes = !$in_single_quotes;

                    break;

                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes) {
                        $is_close = true;
                        $is_open = false;
                        $grab_open = false;
                    }

                    break;

                case ' ':
                    if ($is_open) {
                        $grab_open = false;
                    } else {
                        $stripped++;
                    }

                    break;

                case '>':
                    if ($is_open) {
                        $is_open = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = '';
                    } elseif ($is_close) {
                        $is_close = false;
                        array_pop($tags);
                        $tag = '';
                    }

                    break;

                default:
                    if ($grab_open || $is_close) {
                        $tag .= $symbol;
                    }

                    if (!$is_open && !$is_close) {
                        $stripped++;
                    }
            }

            $i++;
        }

        while ($tags) {
            $result .= '</' . array_pop($tags) . '>';
        }

        return $result;
    }
}
