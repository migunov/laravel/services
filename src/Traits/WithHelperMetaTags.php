<?php

namespace Migunov\Services\Traits;

use DOMDocument;
use Exception;
use Illuminate\Support\Str;

trait WithHelperMetaTags
{
    public static function getMetaTags(string $urlOrContent, bool $noThrow = true): array
    {
        libxml_use_internal_errors(true);

        $html = $urlOrContent;

        if (Str::isUrl($urlOrContent)) {
            try {
                $html = self::httpClient()->get($urlOrContent)->body();
            } catch (Exception $e) {
                if ($noThrow) {
                    return [];
                }

                throw $e;
            }
        }

        $result = [];
        $dom = new DOMDocument();

        try {
            if (!$dom->loadHTML($html)) {
                return $result;
            }
        } catch (Exception $e) {
            return $result;
        }

        self::getMetaTagsTitle($dom, $result);
        self::getMetaTagsFromMeta($dom, $result);
        self::getMetaTagsFromLink($dom, $result);

        return $result;
    }

    private static function getMetaTagsTitle(DOMDocument $dom, array &$result): void
    {
        $title = $dom->getElementsByTagName('title');

        $result['title'] = $title->count() > 0 ? $title->item(0)->textContent : '';
    }

    private static function getMetaTagsFromMeta(DOMDocument $dom, array &$result): void
    {
        $metas = $dom->getElementsByTagName('meta');

        /** @var DOMElement */
        foreach ($metas as $meta) {
            $key = $meta->getAttribute('name') ?: $meta->getAttribute('property');

            if ($key) {
                if (isset($result[$key])) {
                    if (!is_array($result[$key])) {
                        $result[$key] = [$result[$key]];
                    }

                    $result[$key][] = $meta->getAttribute('content');
                } else {
                    $result[$key] = $meta->getAttribute('content');
                }
            }
        }
    }

    private static function getMetaTagsFromLink(DOMDocument $dom, array &$result): void
    {
        $needLinks = ['alternate', 'icon'];
        $links = $dom->getElementsByTagName('link');

        /** @var DOMElement */
        foreach ($links as $link) {
            $key = $link->getAttribute('rel');

            if ($key && in_array($key, $needLinks)) {
                if (isset($result[$key])) {
                    if (!is_array($result[$key])) {
                        $result[$key] = [$result[$key]];
                    }

                    $result[$key][] = $link->getAttribute('href');
                } else {
                    $result[$key] = $link->getAttribute('href');
                }
            }
        }
    }
}
