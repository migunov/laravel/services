<?php

namespace Migunov\Services\Traits;

use Intervention\Image\Interfaces\ImageInterface;

trait WithImageCrop
{
    public static function crop(
        string $path,
        int $width = 1200,
        int $height = 600,
        float $x = 0,
        float $y = 0,
        ?string $targetPath = null
    ): void {
        /** @var array */
        $result = self::initImage($path, $targetPath);

        if ($result) {
            self::cropNoSave($result['image'], $width, $height, $x, $y)
                ->save($result['target'], 90);
        }
    }

    public static function cropAndResize(
        string $path,
        int $width = 1200,
        int $height = 600,
        float $x = 0,
        float $y = 0,
        int $newWidth = 300,
        int $newHeight = 300,
        ?string $targetPath = null
    ): void {
        /** @var array */
        $result = self::initImage($path, $targetPath);

        if (!$result) {
            return;
        }

        $image = self::cropNoSave($result['image'], $width, $height, $x, $y);
        list($newWidth, $newHeight) = self::initSize($result['image'], $newWidth, $newHeight);

        if ($image->width() != $newWidth && $image->height() != $newHeight) {
            if ($image->width() >= $image->height()) {
                $image->scale(width: $newWidth);
            } else {
                $image->scale(height: $newHeight);
            }
        }

        $image->save($result['target'], 90);
    }

    public static function cropNoSave(
        ImageInterface $image,
        int $width = 1200,
        int $height = 600,
        float $x = 0,
        float $y = 0
    ): ImageInterface|null {
        list($width, $height) = self::initSize($image, $width, $height);

        return $image->crop($width, $height, $x, $y);
    }
}
