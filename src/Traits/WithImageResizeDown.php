<?php

namespace Migunov\Services\Traits;

trait WithImageResizeDown
{
    public static function resizeDown(
        string $path,
        int $width = 1200,
        int $height = 600,
        ?string $targetPath = null
    ): void {
        /** @var array */
        $result = self::initImage($path, $targetPath);

        if (!$result) {
            return;
        }

        /** @var \Intervention\Image\Interfaces\ImageInterface */
        $image = $result['image'];
        $target = $result['target'];

        if ($width <= 0) {
            $width = $image->width();
        }

        if ($height <= 0) {
            $height = $image->height();
        }

        $image->resizeDown($width, $height)->save($target, 90);
    }
}
