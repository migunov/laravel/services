<?php

namespace Migunov\Services;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Migunov\Services\Traits\WithHelperMetaTags;
use Migunov\Services\Traits\WithHtmlCut;

class Helper
{
    use WithHelperMetaTags;
    use WithHtmlCut;

    public const USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36';

    /**
     * Extract file extension from path. For example: "storage/stores/logo.svg" -> ".svg"
     */
    public static function fileExtension(string $path): string
    {
        $ext = explode('?', $path, 2)[0];

        $extData = explode('\\', $ext);
        $ext = array_pop($extData);

        $extData = explode('/', $ext);
        $ext = array_pop($extData);

        preg_match('#.+(\..+)$#', $ext, $matches);

        return $matches[1] ?? '';
    }

    public static function fileExtensionFromMime(string $contentType): string
    {
        if (strpos($contentType, 'image/') === false) {
            return '';
        }

        $ext = explode('/', $contentType, 2)[1];
        $ext = str_ireplace('+xml', '', $ext); // image/svg+xml

        return '.' . $ext;
    }

    public static function host(string $url, bool $onlyDomain = true): string
    {
        return $onlyDomain
                ? preg_replace('#^https?\://([^/?]+).*#', '$1', $url)
                : preg_replace('#^(http?s://[^/?]+).*#', '$1', $url);
    }

    public static function httpClient(): PendingRequest
    {
        return Http::withUserAgent(self::USER_AGENT);
    }

    /**
     * Clean string values in data.
     */
    public static function sanitizeData(array $data): array
    {
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $data[$key] = trim($value);
            }
        }

        return $data;
    }

    public static function socialName(string $url): string
    {
        if (!Str::isUrl($url)) {
            return '';
        }

        $socials = [
            'facebook' => 'facebook.com',
            'github' => 'github.com',
            'instagram' => 'instagram.com',
            'linkedin' => 'linkedin.com',
            'mastodon' => 'mastodon',
            'x-twitter' => 'twitter.com',
            'youtube' => 'youtube.com',
        ];

        foreach ($socials as $key => $host) {
            if (preg_match('#' . $host . '#i', $url)) {
                return $key;
            }
        }

        $host = preg_replace('#https?://([^\/]+?)(/.+)?$#i', '$1', $url);

        return str_ireplace('www.', '', $host);
    }

    /**
     * Split string to array where values will be trimmed
     * from spaces, tabs and same symbols.
     */
    public static function stringToArray(
        string $value,
        string $separator = ','
    ): array {
        return $separator
            ? self::sanitizeData(explode($separator, $value))
            : [$value];
    }

    public static function timeFormat(int $seconds): string
    {
        if ($seconds < 10) {
            return '';
        }

        if ($seconds <= 60) {
            return '1 mimute';
        }

        if ($seconds < 3600) {
            $minutes = round($seconds / 60);

            return $minutes . ($minutes < 2 ? 'minute' : ' minutes');
        }

        if ($seconds < 86400) {
            $hours = round($seconds / 3600);
            $minutes = round(($seconds % 3600) / 60);

            return "{$hours} hr. {$minutes} min.";
        }

        $days = round($seconds / 86400);
        $hours = round(($seconds % 86400) / 3600);
        $minutes = round((($seconds % 86400) % 3600) / 60);

        return "{$days} d. {$hours} hr. {$minutes} min.";
    }
}
