<?php

namespace Migunov\Services;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Intervention\Image\Interfaces\ImageInterface;
use Migunov\Services\Traits\WithImageCover;
use Migunov\Services\Traits\WithImageCrop;
use Migunov\Services\Traits\WithImageFromUrlPage;
use Migunov\Services\Traits\WithImageResize;
use Migunov\Services\Traits\WithImageResizeDown;

class ImageService
{
    use WithImageFromUrlPage;
    use WithImageCover;
    use WithImageCrop;
    use WithImageResize;
    use WithImageResizeDown;

    public const IMAGE_EXTENSIONS = ['png', 'jpeg', 'jpg', 'svg', 'gif', 'webp', 'tiff'];

    /**
     * @param string $driver "gd" or "imagick". "gd" by default.
     *
     * Read documentation https://image.intervention.io
     */
    public static function getManager(string $driver = 'gd'): ImageManager
    {
        return $driver == 'imagick'
                ? ImageManager::imagick()
                : ImageManager::gd();
    }

    public static function isImageFile(string $path): bool
    {
        $imageExts = implode('|', self::IMAGE_EXTENSIONS);

        return !!preg_match('#\.(' . $imageExts . ')$#', $path);
    }

    private static function initImage(string $path, ?string $targetPath = null): array
    {
        $abspath = Storage::disk('public')->path($path);
        $target = $targetPath ? Storage::disk('public')->path($targetPath) : $abspath;
        $image = ImageManager::gd();

        try {
            $image = $image->read($abspath);
        } catch (Exception) {
            return [];
        }

        return [
            'image' => $image,
            'target' => $target,
        ];
    }

    private static function initSize(ImageInterface $image, int $width, int $height): array
    {
        return [
            $width <= 0 ? $image->width() : $width,
            $height <= 0 ? $image->height() : $height,
        ];
    }
}
